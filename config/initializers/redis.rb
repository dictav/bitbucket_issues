if uri = ENV['REDISTOGO_URL']
  uri = URI.parse uri
  _opts = %w(host port password).map{|k| [k, uri.send(k)]}.flatten
  options = Hash[*_opts]
end

REDIS = Redis.new options || {}
REDIS.ping
