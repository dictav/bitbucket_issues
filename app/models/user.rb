class User < ActiveRecord::Base
  def self.create_with_omniauth(auth)  
    create!do |user|  
      user.provider = auth["provider"]  
      logger.debug auth
    end  
  end  
end
