class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  before_filter :check_auth

  def check_auth
    key = params[:token]
    unless key and info = REDIS.get(key)
      redirect_to "/auth/bitbucket"
      return
    end

    REDIS.expire key, (ENV['EXPIRE_IN'] || 60*60*2 )
    @uid, @access_token, @access_secret, = info.split ':'
  end
end
