class SessionsController < ApplicationController

  before_filter :check_auth, :except => 'create'
  respond_to :json
  def create  
    auth = request.env["omniauth.auth"]  
    access_token = auth['extra']['access_token']
    info = "#{auth['uid']}:#{access_token.token}:#{access_token.secret}:#{Time.now}"
    key = Digest::SHA1.hexdigest info
    REDIS.set key, info

    REDIS.expire key, (ENV['EXPIRE_IN'] || 60*60*2 )

    render :json => {token: key}
  end  

end
