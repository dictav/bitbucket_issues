class IssuesController < ApplicationController
  respond_to :json

  def index
    response = issues.get(repository_path)
    render :json => response.body, :status => response.code
  end

  def comments
    path = repository_path + "/#{params['id']}/comments"
    response = issues.get(repository_path)
    render :json => response.body, :status=>response.code
  end

  def new
    options = params.select{|k,v| ["title", "content", "kind", "priority", "milestone", "component"].include? k}
    response = issues.post(repository_path, options)
    render :json => response.body, :status=>response.code
  end

  def open
    options = {:status => 'open'}

    response = issues.put(repository_path + "/#{params['id']}", options)
    render :json => response.body, :status=>response.code
  end

private

  def issues
    c = OAuth::Consumer.new(ENV['BITBUCKET_KEY'], ENV['BITBUCKET_SECRET'], :site => 'https://bitbucket.org/api/1.0')
    
    a = OAuth::AccessToken.new c
    a.token = @access_token
    a.secret = @access_secret
    a
  end

  def repository_path
    '/repositories/' + params['repo'] + '/issues'
  end
end
